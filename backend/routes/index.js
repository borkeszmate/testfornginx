var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.json({
    name: 'Borki'
  });
});

router.get('/user', function(req, res, next) {
  res.json({
    name: 'user'
  });
});

router.get('/other', function(req, res, next) {
  res.json({
    name: 'other'
  });
});

module.exports = router;
